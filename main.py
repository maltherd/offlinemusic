from typing import Optional
import spotipy
import enum
import sqlite3

import secrets
from model import model, Reference, Unique


con = sqlite3.connect("main.db")


@model(con)
class Artist:
    # Fields
    sp_id: Unique[str]
    name: str

    @classmethod
    def deserialize_spotify(cls, d: dict):
        return cls(sp_id=d["id"], name=d["name"])

    def __post_init__(self):
        self._table = "artists"


@model(con)
class Album:
    @enum.unique
    class Type(str, enum.Enum):
        ALBUM = "ALBUM",
        SINGLE = "SINGLE",
        COMPILATION = "COMPILATION",

    # Fields
    sp_id: Unique[str]
    name: str
    first_artist: Reference[Artist]
    best_cover_url: Optional[str]
    type: Type

    @classmethod
    def deserialize_spotify(cls, d: dict):
        try:
            artist = Artist.find(sp_id=d["artists"][0]["id"])
        except ValueError:
            # if not in db, just put them now
            artist = Artist.deserialize_spotify(d["artists"][0])

        return cls(
            sp_id=d["id"],
            name=d["name"],
            first_artist=artist,
            best_cover_url=d["images"][0]["url"],
            type=cls.Type(d["album_type"])
        )


@model(con)
class Track:
    sp_id: Unique[str]
    name: str
    album: Reference[Album]
    first_track_artist: Reference[Artist]
    preview_url: Optional[str]
    disc_number: Optional[int]
    track_number: Optional[int]

    @classmethod
    def deserialize_spotify(cls, d: dict):
        try:
            album = Album.find(sp_id=d["album"]["id"])
        except ValueError:
            # if not in db, just put them now
            album = Album.deserialize_spotify(d["album"])

        try:
            first_track_artist = Artist.find(sp_id=d["artists"][0]["id"])
        except ValueError:
            # if not in db, just put them now
            first_track_artist = Artist.deserialize_spotify(d["artists"][0])

        return cls(
            sp_id=d["id"],
            name=d["name"],
            album=album,
            first_track_artist=first_track_artist,
            preview_url=d["preview_url"],
            disc_number=d["disc_number"],
            track_number=d["track_number"]
        )


scopes = ["user-read-playback-state", "user-read-recently-played", "user-top-read", "user-library-read"]
sp = spotipy.Spotify(
    auth_manager=spotipy.oauth2.SpotifyOAuth(
        client_id=secrets.spotify_client_id,
        client_secret=secrets.spotify_client_secret,
        redirect_uri="http://127.0.0.1:8080/blipbloup/",
        scope=scopes,
    )
)
