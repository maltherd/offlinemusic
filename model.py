import dataclasses
import itertools
import typing
import enum
import copy


T = typing.TypeVar("T")


class Reference(typing.Generic[T]):  # this typing.Generic[T] allows you to write "Reference[Stuff]"
    def __init__(self, target_cls, pk):  # but the T has no presence in runtime, so we have to pass the class again here ;_;
        if not hasattr(target_cls, "_model_is_model") or not target_cls._model_is_model:
            raise ValueError("Reference must be of a model!")

        self._pk_in_absentia = pk
        self._cached_instance = None
        self._target_cls = target_cls

    def __getattr__(self, name):
        if not self._cached_instance:
            self._cached_instance = self._target_cls.load(pk=self._pk_in_absentia)

        return getattr(self._cached_instance, name)

    @property
    def _pk(self):
        if not self._cached_instance:
            return self._pk_in_absentia
        else:
            return self._cached_instance._model_pk


class Unique(typing.Generic[T]):
    pass


def model(db_con, table_name=None):
    """The absolute decorator"""

    def is_reference(f: dataclasses.Field):
        # python typing dinguerie. it's fine since dataclass does it too tho.
        return typing.get_origin(f.type) == Reference

    def unwrap_reference(reference: typing._GenericAlias):
        return typing.get_args(reference)[0]

    def is_unique(f: dataclasses.Field):
        # python typing dinguerie. it's fine since dataclass does it too tho.
        return typing.get_origin(f.type) == Unique

    def unwrap_unique(unique: typing._GenericAlias):
        return typing.get_args(unique)[0]

    def is_optional(f: dataclasses.Field):
        # absolute absolute dinguerie. the problem being that typing.get_origin(Optional[T]) = Union. because Optional[T] is defined as Union[T, NoneType].
        return f.type.__name__ == "Optional"

    def unwrap_optional(optional: typing._GenericAlias):
        return typing.get_args(optional)[0]

    def is_enum(f: dataclasses.Field):
        # First check is a guard against type annotations, which are not types, and thus cannot be issubclass'ed.
        return isinstance(f.type, type) and issubclass(f.type, enum.Enum)

    def unwrap_enum(enumm: type):
        return enumm.__bases__[0]

    # ---

    def deserialize_db(model_cls: type, row: list):
        pk = row.pop(0)

        model_cls_args = []
        for f in dataclasses.fields(model_cls):
            row_elem = row.pop(0)

            f_copy = copy.copy(f)
            if is_unique(f_copy):
                f_copy.type = unwrap_unique(f_copy.type)

            if is_optional(f_copy):
                f_copy.type = unwrap_optional(f_copy.type)

            if is_reference(f_copy):
                target_type = unwrap_reference(f_copy.type)
                model_cls_args.append(Reference(target_type, row_elem))
            elif is_enum(f_copy):
                model_cls_args.append(f_copy.type(row_elem))
            else:
                model_cls_args.append(row_elem)

        ret = model_cls(*model_cls_args)
        ret._model_pk = pk
        return ret

    def serialize_db(model_instance):
        ret = {}
        for f in dataclasses.fields(model_instance):

            f_copy = copy.copy(f)
            if is_unique(f_copy):
                f_copy.type = unwrap_unique(f_copy.type)

            if is_optional(f_copy):
                f_copy.type = unwrap_optional(f_copy.type)

            if is_reference(f_copy):
                ref = getattr(model_instance, f_copy.name)
                ret[f_copy.name] = ref._pk
            elif is_enum(f_copy):
                ret[f_copy.name] = getattr(model_instance, f_copy.name).value
            else:
                ret[f_copy.name] = getattr(model_instance, f_copy.name)

        return ret

    # ---

    def inside(cls):
        cls._model_is_model: typing.ClassVar[bool] = True
        cls._model_table: typing.ClassVar[str] = table_name or cls.__name__.lower() + "s"
        cls._model_pk: int = None

        def save(self):
            cur = db_con.cursor()

            # if I have uninserted references, save them too (recursively)
            for f in dataclasses.fields(self):
                f_copy = copy.copy(f)
                if is_unique(f_copy):
                    f_copy.type = unwrap_unique(f_copy.type)

                if is_optional(f_copy):
                    f_copy.type = unwrap_optional(f_copy.type)

                if is_reference(f_copy):
                    ref = getattr(self, f_copy.name)
                    if ref._pk is None and ref._cached_instance is not None:
                        ref._cached_instance.save()

            if self._model_pk:
                query = f"UPDATE {self._model_table} SET "

                for f in dataclasses.fields(self):
                    query += f"{f.name} = ?, "
                query = query[-2]  # remove trailing comma
                query += "WHERE pk = ?;"

                values = itertools.chain(serialize_db(self).values(), [self._model_pk])
                try:
                    cur.execute(query, tuple(values))
                except Exception as e:
                    print(f"Query: `{query}`")
                    print(f"Values: `{list(values)}`")
                    raise e

            else:
                query = f"INSERT INTO {self._model_table} ("
                for f in dataclasses.fields(self):
                    query += f"{f.name}, "
                query = query[:-2]  # remove trailing comma

                query += ") VALUES ("
                for f in dataclasses.fields(self):  # wew
                    query += "?, "
                query = query[:-2]  # remove trailing comma
                query += ");"

                values = serialize_db(self).values()
                try:
                    cur.execute(query, tuple(values))
                except Exception as e:
                    print(f"Query: `{query}`")
                    print(f"Values: `{tuple(values)}`")
                    raise e

                self._model_pk = cur.lastrowid

            db_con.commit()

        cls.save = save

        @classmethod
        def load(cls2, pk):
            cur = db_con.cursor()

            query = f"SELECT * FROM {cls2._model_table} WHERE pk = ?;"
            try:
                cur.execute(query, (pk,))
            except Exception as e:
                print(f"Query: `{query}`")
                print(f"Values: `{(pk,)}`")
                raise e

            row = cur.fetchone()
            if row is None:
                raise ValueError(f"Model `{cls2.__name__}` with pk=`{pk}` does not exist.")

            return deserialize_db(cls2, list(row))

        cls.load = load

        @classmethod
        def find(cls2, **kwarg):
            if len(kwarg) > 1:
                raise ValueError("find() only allows one condition!")

            field_name, value = list(kwarg.items())[0]

            for f in dataclasses.fields(cls2):
                if f.name != field_name:
                    continue

                if is_unique(f):

                    cur = db_con.cursor()

                    query = f"SELECT * FROM {cls2._model_table} WHERE {field_name} = ?;"
                    try:
                        cur.execute(query, (value,))
                    except Exception as e:
                        print(f"Query: `{query}`")
                        print(f"Values: `{(value,)}`")
                        raise e

                    row = cur.fetchone()
                    if row is None:
                        raise ValueError(f"Model `{cls2.__name__}` with {field_name}=`{value}` does not exist.")

                    return deserialize_db(cls2, list(row))

                else:
                    raise ValueError(f"find() only allows conditions on unique fields! `{field_name}` is not unique!")
            else:
                raise ValueError(f"find() only allows conditions on fields! `{field_name}` is not a field of model `{cls2.__name__}`.")

        cls.find = find

        @classmethod
        def create_table(cls2):
            cur = db_con.cursor()

            query = f"CREATE TABLE {cls2._model_table} (pk INTEGER NOT NULL PRIMARY KEY, "
            query_foreign_keys = ""
            for f in dataclasses.fields(cls2):

                f_is_nullable = False
                f_is_unique = False
                f_copy = copy.copy(f)

                if is_unique(f_copy):
                    f_is_unique = True
                    f_copy.type = unwrap_unique(f_copy.type)

                if is_optional(f_copy):
                    f_is_nullable = True
                    f_copy.type = unwrap_optional(f_copy.type)

                if is_enum(f_copy):
                    if len(f_copy.type.__bases__) != 2:
                        raise TypeError("Enum fields must have a mix-in type! (see https://docs.python.org/3/library/enum.html#others)")
                    f_copy.type = unwrap_enum(f_copy.type)

                if is_reference(f_copy):
                    target_type = unwrap_reference(f_copy.type)

                    query += f"{f_copy.name} INTEGER"
                    if f_is_unique:
                        query += " UNIQUE"
                    if not f_is_nullable:
                        query += " NOT NULL"
                    query += ", "
                    query_foreign_keys += f"FOREIGN KEY({f_copy.name}) REFERENCES {target_type._model_table}(pk), "

                else:
                    f_sql_type = (
                        "TEXT" if f_copy.type == str else
                        "INTEGER" if f_copy.type == int else
                        "REAL" if f_copy.type == float else
                        "BLOB"
                    )

                    if f_is_unique:
                        f_sql_type += " UNIQUE"

                    if not f_is_nullable:
                        f_sql_type += " NOT NULL"

                    query += f"{f_copy.name} {f_sql_type}, "

            query += query_foreign_keys
            query = query[:-2]  # remove trailing comma
            query += ");"

            try:
                cur.execute(query)
                db_con.commit()
            except Exception as e:
                print(f"Query: `{query}`")
                raise e

        cls.create_table = create_table

        def new_setattr(self, name, value):
            """Implement special behavior when setting a reference.
            Wished I could do it with Reference as a descriptor but no..."""

            for f in dataclasses.fields(self):
                if f.name != name:
                    continue

                f_copy = copy.copy(f)
                if is_unique(f_copy):
                    f_copy.type = unwrap_unique(f_copy.type)

                if is_optional(f_copy):
                    f_copy.type = unwrap_optional(f_copy.type)

                if is_reference(f_copy):
                    # Special behavior here.
                    if type(value) == Reference:
                        self.__dict__[name] = value
                    elif type(value) == unwrap_reference(f_copy.type):

                        # Special special case: allow setting an uninitialized Reference to an instance of a model
                        if name not in self.__dict__:
                            self.__dict__[name] = Reference(type(value), None)

                        ref = self.__dict__[name]
                        ref._cached_instance = value
                    else:
                        raise TypeError(f"Cannot set reference `{name}` of type `{unwrap_reference(f_copy.type)}` to value `{value}`")

                else:
                    self.__dict__[name] = value  # setting a non-reference field

                break

            else:
                self.__dict__[name] = value  # setting a non-field

        cls.__setattr__ = new_setattr

        return dataclasses.dataclass(cls, repr=True)

    return inside
